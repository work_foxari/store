<?php
class ModelAccountNewsletter extends Model {

	public function addNewsletter($email) {
		$sql = "INSERT INTO " . DB_PREFIX . "newsletter SET email = '" . $this->db->escape($email) . "', status = '1', date_added = NOW()";
		$this->db->query($sql);

	}

	public function getTotalNewsletterByEmail($email) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "newsletter WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}
