<?php

class ModelCheckoutQuickorder extends Model
{

    public function addOrder($data)
    {

        $sql = "
          INSERT INTO " . DB_PREFIX . "order
          SET 	telephone = '" . $this->db->escape($data['phone']) . "',
          invoice_prefix='INV-2018',
          store_url='{$_SERVER['HTTP_HOST']}',
          custom_field='[]',
          customer_group_id='1',
          shipping_code='flat.flat',
          store_id='0',
          store_name='Your Store',
          firstname='" . $this->db->escape($data['name']) . "',
          email='" . $this->db->escape($data['email']) . "',
          total = '" . $data['total'] . "',
          comment = '" . $this->db->escape($data['comment']) . "',
          order_status_id = '14',
          currency_id='1',
          currency_code='UAH',
          currency_value='1',
          payment_method='Оплата при доставке',
          payment_code='cod',
          date_added = NOW(),
          date_modified = NOW()
        ";

        $this->db->query($sql);

        $order_id = $this->db->getLastId();

        $sql = "
          INSERT INTO " . DB_PREFIX . "order_product
          SET 	order_id = '" . $order_id . "',
          product_id = '" . (int)$data['product_id'] . "',
          NAME = '" . $data['product_name'] . "',
          model = '" . $data['product_model'] . "',
          quantity = '1',
          price = '" . $data['total'] . "',
          total = '" . $data['total'] . "'
        ";

        $this->db->query($sql);

        $order_id = $this->db->getLastId();

        return $order_id;
    }

    public function addOrderCart($data)
    {


        $sql = "
          INSERT INTO " . DB_PREFIX . "order
          SET 	telephone = '" . $this->db->escape($data['contact']) . "',
          invoice_prefix='INV-2018',
          store_url='{$_SERVER['HTTP_HOST']}',
          custom_field='[]',
          customer_group_id='1',
          shipping_code='flat.flat',
          store_id='0',
          store_name='Your Store',
          firstname='" . $data['name'] . "',
          total = '" . $data['total'] . "',
          order_status_id = '1',
          currency_id='1',
          currency_code='".$this->currency->getCode()."',
          currency_value='".$this->currency->getValue($this->currency->getCode())."',
          payment_method='Оплата при доставке',
          payment_code='cod',
          date_added = NOW(),
          date_modified = NOW()
        ";

        $this->db->query($sql);

        $order_id = $this->db->getLastId();

        foreach ($data['products'] as $product) {
            $sql = "
              INSERT INTO " . DB_PREFIX . "order_product SET
              `order_id` = '" . $order_id . "',
              `product_id` = '" . (int)$product['product_id'] . "',
              `NAME` = '" . addslashes($product['product_name']) . "',
              `model` = '" . addslashes($product['product_model'] ). "',
              `quantity` = '" . $product['quantity'] . "',
              `price` = '" . $product['price'] . "',
              `total` = '" . $product['total'] . "'
            ";
            $this->db->query($sql);

        }


        $order_id = $this->db->getLastId();

        return $order_id;
    }

    public function updateQuantProducts($product_id, $new_quantity){
      $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '".$new_quantity."' WHERE product_id = '" . (int)$product_id . "'");
    }

}

?>