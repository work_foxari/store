<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = '%s';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';
$_['text_page']          = 'страница';

$_['text_delivery']          = 'Доставка и оплата';
$_['text_garanty']          = 'Гарантия и качество';
$_['text_about_as']          = 'О нас';
$_['text_cooperation']          = 'Сотрудничество';
$_['text_contact']          = 'Контакты';
$_['text_catalog']          = 'Каталог';

