<?php
// Heading
$_['heading_title']  = 'Контакты';

// Text
$_['text_location']  = 'Офис:';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Форма связи';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'тел:';
$_['text_fax']       = 'моб:';
$_['text_open']      = 'Режим работы:';
$_['text_comment']   = 'Комментарий';
$_['text_success']   = '<p>Ваш запрос был успешно отправлен администрации магазина!</p>';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'E-Mail:';
$_['entry_enquiry']  = 'Сообщение';

// Email
$_['email_subject']  = 'Сообщение %s';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-Mail указан некорректно!';
$_['error_enquiry']  = 'Сообщение должно быть от 10 до 3000 символов!';
