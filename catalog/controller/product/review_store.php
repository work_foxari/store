<?php
class ControllerProductReviewstore extends Controller {
    public function index() {
        $this->load->language('product/product');
        $data['entry_review'] = $this->language->get('entry_review');
        $this->load->model('catalog/review');
        $data['review_status'] = $this->config->get('config_review_status');
        $data['text_reviews_page'] = $this->language->get('text_reviews_page');
        $data['text_no_reviews'] = $this->language->get('text_no_reviews');
        $data['text_answer'] = $this->language->get('text_answer');
        $data['text_write'] = $this->language->get('text_write');
        $data['text_note'] = $this->language->get('text_note');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_rating'] = $this->language->get('entry_rating');
        $data['entry_bad'] = $this->language->get('entry_bad');
        $data['entry_good'] = $this->language->get('entry_good');
        $data['button_continue'] = $this->language->get('button_continue');
        if ($this->customer->isLogged()) {
            $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
        } else {
            $data['customer_name'] = '';
        }
        if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
            $data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
        } else {
            $data['captcha'] = '';
        }
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );
        if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
            $data['review_guest'] = true;
        } else {
            $data['review_guest'] = false;
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $data['reviews'] = array();

        $review_total = $this->model_catalog_review->getAllTotalReviews();

        $results = $this->model_catalog_review->getAllReviews(($page - 1) * 5, 5);
        $sort_result = array();
        foreach ($results as $key => $res) {
            if ($res['product_id'] != 0) {
                if(!isset($sort_result[$res['product_id']])){
                    $product_info = $this->model_catalog_review->getProductInfo($res['product_id']);
                    if ($product_info['image']) {
                        $product_info['image'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                    } else {
                        $product_info['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                    }
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $product_info['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $product_info['price'] = false;
                    }
                    if (!empty($product_info)) {
                        $sort_result[$res['product_id']]['product_info'][] = $product_info;
                    }
                    $sort_result[$res['product_id']]['reviews'][] = $res;
                } else{
                    $sort_result[$res['product_id']]['reviews'][] = $res;
                }
            } else{
                $sort_result[] = $res;
            }
        }

        $data['reviews'] = $sort_result;
        $pagination = new Pagination();
        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = 5;
        $pagination->url = $this->url->link('product/product/reviewstore', '&page={page}');
        $url = '';
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));
        $data['breadcrumbs'][] = array(
            'text' => $data['text_reviews_page'],
            'href' => $this->url->link('product/review_store', $url)
        );
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('product/reviewstore', $data));
    }

    public function write() {
        $this->load->language('product/product');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                $json['error'] = $this->language->get('error_name');
            }

            if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
                $json['error'] = $this->language->get('error_text');
            }

            if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
                $json['error'] = $this->language->get('error_rating');
            }

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

                if ($captcha) {
                    $json['error'] = $captcha;
                }
            }

            if (!isset($json['error'])) {
                $this->load->model('catalog/review');
                $this->model_catalog_review->addReview(0, $this->request->post);

                $json['success'] = $this->language->get('text_success');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}