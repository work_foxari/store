<?php
class ControllerProductCatalog extends Controller {
    public function index() {
        $this->load->language('product/category');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );
        //TODO
        $data['breadcrumbs'][] = array(
            'text' => 'Каталог',
            'href' => $this->url->link('product/catalog')
        );

        $category = $this->model_catalog_category->getAllCategory();
        foreach ($category as $key => $cat) {
            $children_data = array();
            $children = $this->model_catalog_category->getCategories($cat['category_id']);
            if ($cat['image']) {
                $category[$key]['thumb'] = $this->model_tool_image->resize($cat['image'], '265', '180');
                //$this->document->setOgImage($data['thumb']);
            } else {
                $category[$key]['thumb'] = '';
            }
            $category[$key]['href'] = $this->url->link('product/category', 'path=' . $cat['category_id']);
        }
        $data['category'] = $category;

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $this->response->setOutput($this->load->view('product/catalog', $data));
    }
}