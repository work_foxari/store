<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>

<?php if ($noindex) { ?>
<!-- OCFilter Start -->
<meta name="robots" content="noindex,nofollow" />
<!-- OCFilter End -->
<?php } ?>

<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">

              <script src="https://api.fondy.eu/static_common/v1/checkout/ipsp.js"></script>

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<nav id="top" class="hidden-xs">
  <div class="container">

    <div class="phones pull-left">
      <a class="ico" href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a>
      <a class="number" href="tel:0442990990">(044) 299-09-90</a>
      <a class="number" href="tel:0964698584">(096) 469-85-84</a>
    </div>


    <div class="pull-right">
      <ul class="list-inline">
        <li class="dropdown">
          <a href="" class="top-link dropdown-toggle" data-toggle="dropdown">График работы</a>
          <ul class="dropdown-menu dropdown-menu-right grafik">
            <li>Пн - Пт: 09:00 - 18:00</li>
            <li>Сб: 09:00 - 16:00</li>
          </ul>
        </li>
        <li>
          <a href="<?php echo $cooperation; ?>" class="top-link"><?php echo $text_cooperation; ?></a>
        </li>
        <li>
          <a href="<?php echo $reviews; ?>" class="top-link">Отзывы</a>
        </li>
      </ul>



      <?php //echo $currency; ?>
      <?php //echo $language; ?>
      <div id="google_translate_element"></div>
      <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
        }
      </script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </div>

    <div id="top-links" class="nav pull-left" style="display: none;">
      <ul class="list-inline">
        <li>
          <a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a>
          <span class="hidden-xs hidden-sm hidden-md">(044) 299-09-90 <?php //echo $telephone; ?></span>
          <span class="hidden-xs hidden-sm hidden-md">(096) 469-85-84</span>
        </li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav>

<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-xs-12">
        <div id="logo">
          <?php if ($logo) { ?>
            <?php if ($home == $og_url) { ?>
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
            <?php } else { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
            <?php } ?>
          <?php } else { ?>
            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-6 search"><?php echo $search; ?></div>
      <div class="col-sm-2 text-right buttons">

        <a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="total"><?php echo $text_wishlist; ?></span></a>

        <?php echo $cart; ?>

        <div id="loginTop" class="dropdown">
          <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle ico ico-user" data-toggle="dropdown"></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>

      </div>
    </div>
  </div>
</header>

<div id="menuTop" class="">
  <div class="container">

    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileCategory" aria-expanded="false" aria-controls="mobileCategory">
      <span class="sr-only"><?php echo $text_catalog; ?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <a href="<?php echo $catalog; ?>" class="catalogue hidden-xs"><?php echo $text_catalog; ?></a>

    <?php if ($categories) { ?>
    <div class="module-category popup">

      <ul class="menu">

        <?php foreach ($categories as $category) { ?>
          <?php $class = '';
            if ($category['children']) $class .= ' parent';
          ?>

        <li>
          <a href="<?php echo $category['href']; ?>" class="<?=$class?>"><?=$category['name']; ?></a>

        <?php if ($category['children']) { ?>

          <ul class="children">
            <?php 
              foreach ($category['children'] as $child) { 

              $class = '';
              if ($child['children2']) $class .= ' parent';
            ?>
            <li>
              <a href="<?=$child['href']; ?>" class="<?=$class?>"><?=$child['name']; ?></a>

              <?php if ($child['children2']) { ?>
              <div class="children">
                <?php 
                  foreach ($child['children2'] as $child2) { 
                ?>
                    <a href="<?=$child2['href']; ?>"><?=$child2['name']; ?></a>

                <?php } ?>
              </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
        <?php } ?>
        </li>
        <?php } ?>
      </ul>

    </div>
    <?php } ?>

    <a href="<?php echo $garanty; ?>" class="item hidden-xs"><?php echo $text_garanty; ?></a>
    <a href="<?php echo $delivery; ?>" class="item hidden-xs"><?php echo $text_delivery; ?></a>
    <a href="<?php echo $contact; ?>" class="item hidden-xs"><?php echo $text_contact; ?></a>
    <a href="<?php echo $about_as; ?>" class="item hidden-xs"><?php echo $text_about_as; ?></a>

  </div>
</div>


<div id="mobileCategory" class="menu-category hidden-sm hidden-md hidden-lg navbar-collapse collapse" aria-multiselectable="true">
  <div class="panel">
    <a class="grey parent" role="button" data-toggle="collapse" href="#mobile_collapse_catalogue" aria-expanded="false" aria-controls="mobile_collapse_catalogue"><?php echo $text_catalog; ?></a>

    <div id="mobile_collapse_catalogue" class="panel-collapse collapse" role="tabpanel">
      <?php foreach ($categories as $id => $category) { ?>
      <div class="panel" role="tab">
        <?php if ($category['children']) { ?>
          <a class="parent" role="button" data-toggle="collapse" data-parent="#mobile_collapse_catalogue" href="#mobile_collapse_<?=$id; ?>" aria-expanded="false" aria-controls="mobile_collapse_<?=$id; ?>"><?=$category['name']; ?></a>

            <div id="mobile_collapse_<?=$id; ?>" class="panel-collapse collapse" role="tabpanel">
            <?php foreach ($category['children'] as $child_id => $child) { ?>
              <div>
                <?php if ($child['children2']) { ?>
                  <a class="parent" role="button" data-toggle="collapse" href="#mobile_collapse_second_<?=$child_id; ?>" aria-expanded="false" aria-controls="mobile_collapse_second_<?=$child_id; ?>"><?=$child['name']; ?></a>

                  <div id="mobile_collapse_second_<?=$child_id; ?>" class="panel-collapse collapse" role="tabpanel" style="padding-left: 20px;">
                    <?php foreach ($child['children2'] as $child2) { ?>
                      <a href="<?=$child2['href']; ?>"><?=$child2['name']; ?></a>
                    <?php } ?>
                  </div>
                <?php } else { ?>
                  <a href="<?=$child['href']; ?>"><?=$child['name']; ?></a>
                <?php } ?>
              </div>
            <?php } ?>
            </div>
          <?php } else { ?>
            <a href="<?=$category['href']; ?>" class=""><?=$category['name']; ?></a>
          <?php } ?>
        </div>
      <?php } ?>
    </div>

    <a href="/about" class="grey">О нас</a>
    <a href="/oplata-i-dostavka" class="grey">Оплата и доставка</a>
    <a href="/contact-us" class="grey">Контакты</a>

    <button type="button" class="close-mobile-menu" data-toggle="collapse" data-target="#mobileCategory" aria-controls="mobileCategory">
      <i class="fa fa-times"></i>
    </button>

  </div>


</div>