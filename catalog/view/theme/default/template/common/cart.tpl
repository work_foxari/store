<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="modal" data-target="#cartModal" data-loading-text="<?php echo $text_loading; ?>" class="btn-cart dropdown-toggle ico ico-cart"><span id="cart-total"><?php echo $text_items; ?></span>
  </button>

  <!-- Modal -->
  <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-left">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="cartModalLabel">Корзина</h4>
        </div>
        <div class="modal-body">

            <?php if ($products || $vouchers) { ?>

              <table class="table">
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if ($product['thumb']) { ?>
                    <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                    </a>
                    <?php } ?></td>
                  <td class="text-left title">
                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                    <?php if ($product['option']) { ?>
                    <?php foreach ($product['option'] as $option) { ?>
                    <br />
                    - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                    <?php } ?>
                    <?php } ?>
                    <?php if ($product['recurring']) { ?>
                    <br />
                    - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                    <?php } ?>
                  </td>
                  <td class="text-right qty">x <?php echo $product['quantity']; ?></td>
                  <td class="text-right price"><?php echo $product['total']; ?></td>
                  <td class="text-center action">
                    <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-delete"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                  <td class="text-center"></td>
                  <td class="text-left title"><?php echo $voucher['description']; ?></td>
                  <td class="text-right qty">x&nbsp;1</td>
                  <td class="text-right price"><?php echo $voucher['amount']; ?></td>
                  <td class="text-center action">
                    <button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-delete"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php } ?>
              </table>


              <div>
                <table class="no-border" width="100%">
                  <?php foreach ($totals as $total) { ?>
                  <tr>
                    <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                    <td class="text-right price"><?php echo $total['text']; ?></td>
                  </tr>
                  <?php } ?>
                </table>

                <div class="buttons">
                  <button type="button" class="continue pull-left" data-dismiss="modal" aria-label="Close">Продолжить покупки</button>

                  <a class="btn btn-dark" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
                </div>
              </div>

            <?php } else { ?>

              <p class="text-center"><?php echo $text_empty; ?></p>

            <?php } ?>


        </div>
      </div>
    </div>
  </div>

</div>
