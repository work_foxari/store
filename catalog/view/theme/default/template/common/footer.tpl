<footer>
  <div class="container">
    <h3 class="footer-title">Наши контакты</h3>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <ul class="list-unstyled">
          <li class="item-title">Свяжитесь с нами</li>
          <li class="item"><a href="<?php echo $contact; ?>">Карта проезда</a></li>
          <li class="item"><a href="tel:0442990990">тел: (044) 299-09-90</a></li>
          <li class="item"><a href="tel:0964698584">моб: (096) 469-85-84</a></li>
          <li class="item"><a href="mailto:lyvyni@ukr.net">e-mail: lyvyni@ukr.net</a></li>
          <li>&nbsp;</li>
          <li class="socials">
            <a href="" class="ico ico-viber"></a>
            <a href="" class="ico ico-skype"></a>
          </li>
        </ul>
      </div>
      <div class="col-sm-6 col-md-3">
        <ul class="list-unstyled">
          <li class="item-title">График работы</li>
          <li class="item">пн-чт     9.00       17.30</li>
          <li class="item">пятница     9.00       16.30</li>
          <li class="item">сб-вс   выходной</li>
        </ul>
      </div>
      <div class="col-sm-6 col-md-3">
        <ul class="list-unstyled">
          <li class="item-title">Покупателям</li>
          <li class="item"><a href="<?php echo $cooperation; ?>"><?php echo $text_cooperation; ?></a></li>
          <li class="item"><a href="<?php echo $opt; ?>"><?php echo $text_opt; ?></a></li>
          <li class="item"><a href="<?php echo $garanty; ?>"><?php echo $text_garanty; ?></a></li>
          <li class="item"><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-6 col-md-3 text-right">
        <h4 class="item-title">Мы в соц. сетях</h4>
        <div class="socials">
          <a href="" class="ico ico-fb"></a>
          <a href="" class="ico ico-yt"></a>
          <a href="" class="ico ico-gp"></a>
        </div>
        <div class="suscribe">
          <form action="" id="newsletter">
            <label for="">Подписка</label>
            <input class="input-email" type="text" name="email" placeholder="E-MAIL">
            <input class="submit" id="newsletter_button" type="submit" value="OK">
          </form>
        </div>

      </div>




<?php if (FALSE): ?>
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>

<?php endif ?>
    </div>
  </div>
</footer>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

<script>
  $(document).ready(function() {

    //Mobile menu fixed body scroll
    $('#mobileCategory').on('shown.bs.collapse', function () {
      $('body').addClass('fixed');
    })
    $('#mobileCategory').on('hidden.bs.collapse', function () {
      $('body').removeClass('fixed');
    })
  });
</script>
</body></html>