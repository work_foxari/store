<?php echo $header; ?>
<div class="container not-found">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <div class="content">
        <div class="col-sm-6 text-center">
          <img src="/catalog/view/theme/default/image/404.png" alt="404">
        </div>
        <div class="col-sm-6">
          <h1><?php echo $heading_title; ?></h1>
          <p><?php echo $text_error; ?></p>

          <p>Возможно Вам нужны следующие страницы:</p>
          <ul class="list-unstyled">
            <li><a href="/">На главную </a></li>
            <li><a href="<?php echo $catalog; ?>">Каталог</a></li>
            <li><a href="<?php echo $contact; ?>">Контакты</a></li>
            <li><a href="<?php echo $about_as; ?>">О нас</a></li>
          </ul>
        </div>
      </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
