<?php echo $header; ?>
<div class="container information">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      <div class="content">
        <?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
        <?php echo $description; ?>
      </div>
    </div>
    <?php echo $column_right; ?>
  </div>
  <div class="row information">
    <?php echo $content_bottom; ?>
  </div>
</div>
<?php echo $footer; ?>