<?php echo $header; ?>
<div class="container information">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      <div class="content">
        <?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
        <span class="blue-title"><?php echo $text_location; ?></span>

        <address>
        <?php echo $address; ?>
        </address>

        <?php if ($open) { ?>
        <span class="blue-title"><?php echo $text_open; ?></span><br />
        <?php echo $open; ?>
        <br />
        <?php } ?>
        <?php if ($comment) { ?>
        <span class="blue-title"><?php echo $text_comment; ?></span>
        <?php echo $comment; ?>
        <?php } ?>
        <br />

        <span class="blue-title"><?php echo $text_telephone; ?></span>
        <?php echo $telephone; ?>
        <?php if ($fax) { ?>
        <span class="blue-title">  <?php echo $text_fax; ?></span>
        <?php echo $fax; ?>
        <?php } ?>
        <br /><br />

        <span class="blue-title uppercase"><?php echo $entry_email; ?></span>
        <a class="uppercase" href="mail-to:lyvyni@ukr.net ">lyvyni@ukr.net </a><br />
        <br />


        <div class="map-holder" style="height: 440px;">
          <div class="map-frame">
            <div class="overlay" onClick="style.pointerEvents='none'"></div>
              <iframe width="100%" height="100%" frameborder="0" style="border:0"
              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDsZdTIWKKY08TkLStpraw-z-Fm-evBYk0&q=<?php echo urlencode($geocode); ?>" allowfullscreen></iframe>
          </div>
        </div>

        <br>


        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal hidden">
          <fieldset>
            <legend><?php echo $text_contact; ?></legend>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
              <div class="col-sm-10">
                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                <?php if ($error_name) { ?>
                <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
              <div class="col-sm-10">
                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
              <div class="col-sm-10">
                <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                <?php if ($error_enquiry) { ?>
                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                <?php } ?>
              </div>
            </div>
            <?php echo $captcha; ?>
          </fieldset>
          <div class="buttons">
            <div class="pull-right">
              <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
            </div>
          </div>
        </form>
      </div>



      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
