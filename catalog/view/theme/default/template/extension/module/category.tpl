<div class="module-category">
  <ul class="menu">

    <?php foreach ($categories as $category) { ?>
      <?php $class = '';
        if ($category['children']) $class .= ' parent';
        if ($category['category_id'] == $category_id) $class .= ' active';
      ?>

    <li>
      <a href="<?php echo $category['href']; ?>" class="<?=$class?>"><?=$category['name']; ?></a>

    <?php if ($category['children']) { ?>

      <ul class="children">
        <?php 
          foreach ($category['children'] as $child) { 

          $class = '';
          if ($child['children2']) $class .= ' parent';
          if ($child['category_id'] == $child_id) $class .= ' active';
        ?>
        <li>
          <a href="<?=$child['href']; ?>" class="<?=$class?>"><?=$child['name']; ?></a>

          <?php if ($child['children2']) { ?>
          <div class="children">
            <?php 
              foreach ($child['children2'] as $child2) { 
              $class = ($child2['category_id'] == $child_id2) ? 'active' : '';
            ?>
                <a href="<?=$child2['href']; ?>" class="<?=$class?>"><?=$child2['name']; ?></a>

            <?php } ?>
          </div>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
    <?php } ?>
    </li>
    <?php } ?>
  </ul>
</div>


