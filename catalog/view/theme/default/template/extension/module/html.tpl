<div class="container module-html">
  <?php if($heading_title) { ?>
    <h2 class="module-title"><?php echo $heading_title; ?></h2>
  <?php } ?>
  <?php echo $html; ?>
</div>
