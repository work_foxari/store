<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">

      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>

      <h1 class="title-page">Каталог</h1>

      <?php echo $content_top; ?>
      <?php if ($category) { ?>
        <div class="row">
        <?php foreach ($category as $cat) { ?>
            <div class="col-sm-6 col-md-3">
              <div class="thumb">
                <a href="<?php echo $cat['href']; ?>">
                  <h3 class="title">
                    <span><?php echo $cat['name']; ?></span>
                  </h3>
                  <div class="image" style="background-image: url('<?php echo $cat['thumb']; ?>');"></div>
                </a>
              </div>
            </div>
        <?php } ?>
        </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>