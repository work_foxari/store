<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      <?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-12">
          <div class="list-group">
            <?php foreach ($categories as $category) { ?>
            <a class="list-group-item" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($products) { ?>
      <div class="row">

        <div class="col-md-4 col-xs-6">
          <div class="form-group input-group input-group-sm select-custom">
            <label class="input-group-addon hidden" for="input-sort"><?php echo $text_sort; ?></label>
            <select id="input-sort" class="form-control" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>

        <div class="col-md-4 col-sm-6 hidden-xs">
          <div class="btn-group btn-group-sm">
            <button type="button" id="list-view" class="btn" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 hidden">
          <div class="form-group">
            <a href="<?php echo $compare; ?>" id="compare-total" class="btn btn-link"><?php echo $text_compare; ?></a>
          </div>
        </div>

        <div class="col-md-4 col-xs-6 text-right">
          <div class="btn-group square" role="group" aria-label="...">
            <?php foreach ($limits as $limits) { ?>
            <?php $class = ($limits['value'] == $limit) ? 'active' : '' ?>
            <a href="<?php echo $limits['href']; ?>" type="button" class="btn <?php echo $class; ?>"><?php echo $limits['text']; ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="row" id="tovars">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <div class="image">
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
              <div class="sticker">
                <?php if ($product['special']) { ?>
                <span class="action">Акция</span>
                <?php } ?>
                <?php if ($product['bestseller']) { ?>
                <span class="hit">Хит</span>
                <?php } ?>
                <?php if ($product['novelty']) { ?>
                <span class="new">Новинка</span>
                <?php } ?>
              </div>
            </div>

            <div class="center">

              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p class="short-desc hidden-list">Артикул: <?php echo $product['sku']; ?></p>
              <?php if ($product['price']) { ?>
              <p class="price hidden-list">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>

              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>

              <p class="hidden-grid"><?php echo $product['description']; ?></p>

            </div>
            <div class="right text-center">

              <?php if ($product['price']) { ?>
              <p class="price hidden-grid">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>

              <div class="number-input">
                <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" ></button>
                <input id="input-quantity_<?php echo $product['product_id']; ?>" class="quantity" min="1" value="1" name="quantity" type="number">
                <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
              </div>


              <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
              </div>

            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php if ($pagination) { ?>
        <div class="row text-center">
          <?php if ($ajax_page == 1){ ?>
          <div class="btn-group square">
            <button class="btn btn-primary" type="button" id="get_products">Показать еще</button>
          </div>
          <?php } ?>
          <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
          <div class="hidden col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      <?php } ?>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>

  $(function(){

  var page_start = 2;
  $('#get_products').on('click', function(){
    var data = {};
        data['action'] = 'get_products_ajax';
        data['page'] = page_start;
    <?php if ($ajax_sort){ ?>
        data['sort'] = '<?php echo $ajax_sort; ?>';
    <?php } ?>
    <?php if ($ajax_order){ ?>
        data['order'] = '<?php echo $ajax_order; ?>';
    <?php } ?>
    <?php if ($ajax_limit){ ?>
        data['limit'] = '<?php echo $ajax_limit; ?>';
        var limit = '<?php echo $ajax_limit; ?>';
    <?php } ?>
    <?php if ($ajax_category_id){ ?>
        data['path'] = '<?php echo $ajax_category_id; ?>';
    <?php } ?>
    <?php if ($ajax_filter_ocfilter){ ?>
        data['filter_ocfilter'] = '<?php echo $ajax_filter_ocfilter; ?>';
    <?php } ?>

    $.ajax({
        url: 'index.php?route=product/category/index',
        method: 'get',
        data: data,
        dataType: 'json',
        beforeSend: function() {
          button = $('#pagination').text();
          $('#pagination').attr('disabled', true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        },
        success: function(data) {
          var html = '';
          for (i in data) {

            html +='<div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">';
            html +='<div class="product-thumb">';
            html +='<div class="image">';
            html +='<a href="'+data[i]['href']+'"><img src="'+data[i]['thumb']+'" alt="'+data[i]['name']+'" title="'+data[i]['name']+'" class="img-responsive"></a>';
            html +='<div class="sticker">';
            html +='</div>';
            html +='</div>';
            html +='<div class="center">';
            html +='<h4><a href="'+data[i]['href']+'">'+data[i]['name']+'</a></h4>';
            html +='<p class="short-desc hidden-list">Артикул: '+data[i]['sku']+'</p>';
            html +='<p class="price hidden-list">';
            if(data[i]['special']) {
              html +='<span class="price-new">'+data[i]['special']+'</span> <span class="price-old">'+data[i]['price']+'</span>';
            } else {
              html += data[i]['price'];
            }
            html +='</p>';
            html +='<div class="rating">';
            for (n = 1; n <= 5; n++) {
              if (data[i]['rating'] < n) {
                html +='<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
              } else {
                html +='<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>';
              }
            }
            html +='</div>';
            html +='<p class="hidden-grid">Артикул: '+data[i]['sku']+'</p>';
            html +='</div>';
            html +='<div class="right text-center">';
            html +='<p class="price hidden-grid">';
              if(data[i]['special']) {
                html +='<span class="price-new">'+data[i]['special']+'</span> <span class="price-old">'+data[i]['price']+'</span>';
              } else {
                html += data[i]['price'];
              }
            html +='</p>';

            html +='<div class="number-input">';
            html +='<button onclick="this.parentNode.querySelector(\'input[type=number]\').stepDown()"></button>';
            html +='<input id="input-quantity_'+data[i]['product_id']+'" class="quantity" min="1" value="1" name="quantity" type="number">';
            html +='<button onclick="this.parentNode.querySelector(\'input[type=number]\').stepUp()" class="plus"></button>';
            html +='</div>';
            html +='<div class="button-group">';
            html +='<button type="button" onclick="cart.add(\''+data[i]['product_id']+'\', \'1\');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">В корзину</span></button>';
            html +='<button type="button" data-toggle="tooltip" title="" onclick="wishlist.add(\''+data[i]['product_id']+'\');" data-original-title="В закладки"><i class="fa fa-heart"></i></button>';
            html +='<button type="button" data-toggle="tooltip" title="" onclick="compare.add(\''+data[i]['product_id']+'\');" data-original-title="В сравнение"><i class="fa fa-exchange"></i></button>';
            html +='</div>';
            html +='</div>';
            html +='</div>';
            html +='</div>';
          }

          if(i < 15 ) {
            $('#get_products').css('display','none');
          }
          $('#tovars').append(html);
          page_start = parseInt(page_start) + 1;
        },
        complete: function() {
          $('.pagination').attr('disabled', false).html(button);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
  })

});

</script>

<?php echo $footer; ?>
