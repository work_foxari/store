<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <h2 class="module-title"><?php echo $text_write; ?></h2>
    <div id="review">
      <form class="form-horizontal" id="form-review">
        <?php if ($review_guest) { ?>

        <div class="form-group required">
          <label class="control-label col-sm-3" for="input-review"><?php echo $entry_review; ?></label>
          <div class="col-sm-9">
            <textarea name="text" rows="5" id="input-review" class="form-control" placeholder="Опишите ваше первое впечатление"></textarea>
            <div class="help-block"><?php echo $text_note; ?></div>
          </div>
        </div>

        <div class="form-group required">
          <label class="control-label col-sm-3" for="input-name"><?php echo $entry_name; ?></label>
          <div class="col-sm-9">
            <div class="row">
              <div class="col-sm-6">
                <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" placeholder="Имя" />
              </div>
              <div class="col-sm-6">
                <input type="text" name="email" placeholder="Введите email для ответа" id="input-name" class="form-control" />
              </div>
            </div>
          </div>
        </div>

        <div class="form-group required">
          <label class="control-label col-xs-3"><?php echo $entry_rating; ?></label>
          <div class="col-xs-9">
            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
            <input type="radio" name="rating" value="1" />
            &nbsp;
            <input type="radio" name="rating" value="2" />
            &nbsp;
            <input type="radio" name="rating" value="3" />
            &nbsp;
            <input type="radio" name="rating" value="4" />
            &nbsp;
            <input type="radio" name="rating" value="5" />
            &nbsp;<?php echo $entry_good; ?></div>
        </div>
        <?php echo $captcha; ?>
        <div class="col-sm-offset-3 col-sm-9 text-center">
          <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
        </div>
        <?php } else { ?>
        <?php echo $text_login; ?>
        <?php } ?>
      </form>
    </div>

    <?php if ($reviews) { ?>
    <div class="reviews-module">
      <?php foreach ($reviews as $review) { ?>
      <?php if(isset($review['product_info'])){ ?>
        <div class="review-item">
            <div class="row product">
                <div class="col-sm-4 col-md-3">
                    <a href="/index.php?route=product/product&product_id=<?php echo $review['product_info'][0]['product_id']; ?>">
                        <img style="width:100%" src="<?php echo $review['product_info'][0]['image']; ?>">
                    </a>
                </div>
                <div class="col-sm-3 col-md-6">
                    <div class="title">
                      <a href="/index.php?route=product/product&product_id=<?php echo $review['product_info'][0]['product_id']; ?>"><?php echo $review['product_info'][0]['name']; ?> </a>
                    </div>
                    <div class="articule">Артикул: <?php echo $review['product_info'][0]['sku']; ?></div>
                </div>
                <div class="col-sm-2 col-md-3">
                    <span class="price"><?php echo $review['product_info'][0]['price']; ?></span>

                    <button class="wishlist" type="button" data-toggle="tooltip" title="add to wishlist" onclick="wishlist.add('<?php echo $review['product_info'][0]['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                </div>
            </div>
            <div class="product-reviews">
              <?php foreach ($review['reviews'] as $review) { ?>
                  <div class="review-single" style="margin-top: 10px;">
                    <div class="title-item">
                      <span class="name"><?php echo $review['author']; ?></span>
                      <span class="date"><?php echo $review['date_added']; ?></span>
                      <span class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($review['rating'] < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } ?>
                        <?php } ?>
                      </span>
                    </div>
                    <div class="text">
                      <?php echo $review['text']; ?>
                    </div>
                    <?php if(!empty($review['text_answer'])){ ?>
                    <div class="answer">
                      <div class="block"><i class="fa fa-commenting" aria-hidden="true"></i> <?php echo $text_answer; ?></div>
                      <div class="text"><?php echo $review['text_answer']; ?></div>
                    </div>
                    <?php } ?>
                  </div>
                <?php } ?>
            </div>
        </div>

      <?php } else{ ?>
          <div class="review-item" style="margin-top: 10px;">
            <div class="title-item">
              <span class="name"><?php echo $review['author']; ?></span>
              <span class="date"><?php echo $review['date_added']; ?></span>
              <span class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </span>
            </div>
            <div class="text">
              <?php echo $review['text']; ?>
            </div>
            <?php if(!empty($review['text_answer'])){ ?>
            <div class="answer">
              <div class="block"><i class="fa fa-commenting" aria-hidden="true"></i> <?php echo $text_answer; ?></div>
              <div class="text"><?php echo $review['text_answer']; ?></div>
            </div>
            <?php } ?>
          </div>
      <?php } ?>
      <?php } ?>
      <div class="text-right"><?php echo $pagination; ?></div>
    </div>
    <?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
    <?php } ?>
</div>
<?php echo $footer; ?>
<script>
$('#button-review').on('click', function() {
    $.ajax({
        url: 'index.php?route=product/review_store/write',
        type: 'post',
        dataType: 'json',
        data: $("#form-review").serialize(),
        beforeSend: function() {
            $('#button-review').button('loading');
        },
        complete: function() {
            $('#button-review').button('reset');
        },
        success: function(json) {
            $('.alert-success, .alert-danger').remove();

            if (json['error']) {
                $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }

            if (json['success']) {
                $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                $('input[name=\'name\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);
            }
        }
    });
    grecaptcha.reset();
});
</script>