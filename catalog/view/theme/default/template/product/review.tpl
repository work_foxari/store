<?php if ($reviews) { ?>
<div class="reviews">
  <span class="title-block">Все отзывы</span>
  <hr>
  <?php foreach ($reviews as $review) { ?>
  <div class="review-item">
    <div class="title-item">
      <span class="name"><?php echo $review['author']; ?></span>
      <span class="date"><?php echo $review['date_added']; ?></span>
      <span class="rating">
        <?php for ($i = 1; $i <= 5; $i++) { ?>
        <?php if ($review['rating'] < $i) { ?>
        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
        <?php } else { ?>
        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
        <?php } ?>
        <?php } ?>
      </span>
    </div>
    <div class="text">
      <?php echo $review['text']; ?>
    </div>
    <?php if(!empty($review['text_answer'])){ ?>
    <div class="answer" style="padding-left: 30px;">
      <b><?php echo $text_answer; ?></b> <?php echo $review['text_answer']; ?>
    </div>
    <?php } ?>
  </div>
  <?php } ?>
  <div class="text-right"><?php echo $pagination; ?></div>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
