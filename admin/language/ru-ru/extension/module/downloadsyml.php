<?php
// Heading
$_['heading_title']    = 'Управление YML/XML загрузками';
$_['heading_title_2']    = 'Добавление нового каталога';

// Text
$_['text_extension']   = 'Модули';
$_['button_add'] = 'Добавить каталог';
$_['button_edit'] = 'Редактировать каталог';
$_['button_delete'] = 'Удалить каталог';