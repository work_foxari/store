<?php
class ModelCustomerNewsletter extends Model {

	public function getNewsletter($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "newsletter WHERE status = '1'";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalNewsletter($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "newsletter WHERE status = '1'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}
