<?php
class ModelExtensionDownloadsyml extends Model {
    public function addCatalog($data){
        $this->db->query("INSERT INTO `" . DB_PREFIX . "ymlsettings` SET `name` = '" . $this->db->escape($data['name_catalog']) . "', `slug` = '" . $this->db->escape($data['url_catalog']) . "'");
    }
    public function getAllCatalog(){
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ymlsettings` ORDER BY id DESC");
        return $query->rows;
    }
    public function getCatalog($catalog_id){
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ymlsettings` WHERE id = '".$catalog_id."'");
        return $query->row;
    }
    public function deleteCatalog($catalog_id){
        $this->db->query("DELETE FROM `" . DB_PREFIX . "ymlsettings` WHERE id = '".$catalog_id."'");
    }
}