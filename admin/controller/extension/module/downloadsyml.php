<?php
class ControllerExtensionModuleDownloadsYML extends Controller {
    public function index() {
        $this->load->language('extension/module/downloadsyml');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');

        $this->getList();
    }
    public function add(){
        $this->load->model('extension/downloadsyml');
        if (!empty($this->request->post['name_catalog'])&&(!empty($this->request->post['url_catalog']))) {
            $this->model_extension_downloadsyml->addCatalog($this->request->post);
        }
        $this->getList();
    }
    public function edit(){
        $this->load->language('extension/module/downloadsyml');
        $this->load->model('extension/downloadsyml');
        if (isset($this->request->get['catalog_id'])) {
           $catalog_info = $this->model_extension_downloadsyml->getCatalog($this->request->get['catalog_id']);
        }
        $url_catalog = $catalog_info['slug'];
        if ((isset($url_catalog))&&(!empty($url_catalog))) {
            //set_time_limit(1000);
            $reader = new XMLReader();
            $reader->open(trim($url_catalog));
            $data = array();
            $i = 0;
            while($reader->read()) {
                if($reader->nodeType == XMLReader::ELEMENT) {
                    // если находим элемент <card>
                    if($reader->localName == 'category') {
                        // считываем аттрибут number
                        $id = $reader->getAttribute('id');
                        $categories_array[$id]['id'] = $id;
                        $categories_array[$id]['parent_id'] = $reader->getAttribute('parentId');
                        if (empty($categories_array[$id]['parent_id'])) {
                            $categories_array[$id]['top'] = 1;
                        }
                        else{
                            $categories_array[$id]['top'] = 0;
                        }
                        $categories_array[$id]['picture'] = $reader->getAttribute('picture');
                        $categories_array[$id]['sort'] = $reader->getAttribute('sort');
                        $categories_array[$id]['meta_title'] = $reader->getAttribute('meta_title');
                        $categories_array[$id]['meta_description'] = $reader->getAttribute('meta_description');
                        $categories_array[$id]['meta_keywords'] = $reader->getAttribute('meta_keywords');
                        $categories_array[$id]['seourl'] = $reader->getAttribute('seourl');
                        $categories_array[$id]['description'] = $reader->getAttribute('description');
                        // читаем дальше для получения текстового элемента
                        $reader->read();
                        if($reader->nodeType == XMLReader::TEXT) {
                            $categories_array[$id]['name'] = $reader->value;
                        }
                        else if($reader->nodeType == XMLReader::CDATA){
                            $categories_array[$id]['name'] = $reader->value;
                        }
                        if (empty($categories_array[$id]['meta_keywords'])) {
                            $categories_array[$id]['meta_h1'] = $categories_array[$id]['name'];
                        }
                        else{
                            $categories_array[$id]['meta_h1'] = $categories_array[$id]['meta_keywords'];
                        }
                    }
                $i++;
                }
            }
        }

        echo '<pre>';
        print_r($categories_array);
        echo '<pre>';
        die('ok');

// Array
// (
//     [id] => 1
//     [slug] => https://skaltair.prom.ua/yandex_market.xml?hash_tag=fa2209224c88d6079ac3eb685eef7873&sales_notes=&product_ids=65624476%2C623952505%2C22519886%2C623959398%2C22519399%2C12409542%2C65444614%2C65624004%2C623963813%2C12368250%2C12351013%2C12370945%2C22518976%2C22520182%2C12370964%2C63246199%2C73196343%2C12358226%2C66277659%2C262483639&group_ids=&label_ids=&exclude_fields=&html_description=0&yandex_cpa=&process_presence_sure=
//     [settings] =>
//     [name] => Тестовий каталог
//     [period] => 0
// )



        echo '<pre>';
        print_r($catalog_info);
        echo '<pre>';
        die('ok');
    }
    public function delete(){
        $this->load->model('extension/downloadsyml');
        if (isset($this->request->get['catalog_id'])) {
           $this->model_extension_downloadsyml->deleteCatalog($this->request->get['catalog_id']);
        }
        $this->getList();
    }
    public function getList(){
        $this->load->language('extension/module/downloadsyml');
        $this->load->model('extension/downloadsyml');
        $url = '';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/downloadsyml', 'token=' . $this->session->data['token'] . $url, true)
        );
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['add'] = $this->url->link('extension/module/downloadsyml/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_title_2'] = $this->language->get('heading_title_2');
        $data['all_catalog'] = $this->model_extension_downloadsyml->getAllCatalog();
        foreach ($data['all_catalog'] as $key => $a_c) {
            $data['all_catalog'][$key]['edit'] = $this->url->link('extension/module/downloadsyml/edit', 'token=' . $this->session->data['token'] . '&catalog_id=' . $a_c['id'] . $url, true);
            $data['all_catalog'][$key]['delete'] = $this->url->link('extension/module/downloadsyml/delete', 'token=' . $this->session->data['token'] . '&catalog_id=' . $a_c['id'] . $url, true);
        }
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/list_download', $data));
    }
}