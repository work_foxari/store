<?php echo $header; ?><?php echo $column_left; ?>
    <!-- Форма додавання нового каталога для завантаження -->
<style>
    /*For tables still need to write 'cellspacing="0"' in code*/
    table {
    border-collapse:collapse;
    border-spacing:0;
    }
    caption, th, td {
    text-align:left;
    font-weight:normal;
    }
    blockquote:before, blockquote:after,
    q:before, q:after {
    content:"";
    }
    blockquote, q {
    quotes:"" "";
    }
.simple-little-table {
    font-family:Arial, Helvetica, sans-serif;
    color:#666;
    font-size:14px;
    text-shadow: 1px 1px 0px #fff;
    background:#eaebec;
    margin:20px;
    border:#ccc 1px solid;
    border-collapse:separate;

    -moz-border-radius:3px;
    -webkit-border-radius:3px;
    border-radius:3px;

    -moz-box-shadow: 0 1px 2px #d1d1d1;
    -webkit-box-shadow: 0 1px 2px #d1d1d1;
    box-shadow: 0 1px 2px #d1d1d1;
    width: 90%;
    margin: auto;
}

.simple-little-table th {
    font-weight:bold;
    padding:21px 25px 22px 25px;
    border-top:1px solid #fafafa;
    border-bottom:1px solid #e0e0e0;

    background: #ededed;
    background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
    background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.simple-little-table th:first-child{
    text-align: left;
    padding-left:20px;
}
.simple-little-table tr:first-child th:first-child{
    -moz-border-radius-topleft:3px;
    -webkit-border-top-left-radius:3px;
    border-top-left-radius:3px;
}
.simple-little-table tr:first-child th:last-child{
    -moz-border-radius-topright:3px;
    -webkit-border-top-right-radius:3px;
    border-top-right-radius:3px;
}
.simple-little-table tr{
    text-align: center;
    padding-left:20px;
}
.simple-little-table tr td:first-child{
    text-align: left;
    padding-left:20px;
    border-left: 0;
}
.simple-little-table tr td {
    padding:5px;
    border-top: 1px solid #ffffff;
    border-bottom:1px solid #e0e0e0;
    border-left: 1px solid #e0e0e0;

    background: #fafafa;
    background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
    background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
.simple-little-table tr:nth-child(even) td{
    background: #f6f6f6;
    background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
    background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
.simple-little-table tr:last-child td{
    border-bottom:0;
}
.simple-little-table tr:last-child td:first-child{
    -moz-border-radius-bottomleft:3px;
    -webkit-border-bottom-left-radius:3px;
    border-bottom-left-radius:3px;
}
.simple-little-table tr:last-child td:last-child{
    -moz-border-radius-bottomright:3px;
    -webkit-border-bottom-right-radius:3px;
    border-bottom-right-radius:3px;
}
.simple-little-table tr:hover td{
    background: #f2f2f2;
    background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
    background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);
}

.simple-little-table a:link {
    color: #666;
    font-weight: bold;
    text-decoration:none;
}
.simple-little-table a:visited {
    color: #999999;
    font-weight:bold;
    text-decoration:none;
}
.simple-little-table a:active,
.simple-little-table a:hover {
    color: #bd5a35;
}
.header_content{
    width: 100%;
    height: 50px;
}
</style>
    <div id="content" style="padding-left: 50px;">
      <div class="page-header">
        <div class="container-fluid">
          <h1><?php echo $heading_title; ?></h1>
          <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="row" style="padding:0;margin:0;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;padding-top:20px;padding-bottom: 20px;">
        <form action="<?php echo $add; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <div class="col-sm-3">
              <h4 style="font-weight: bold;"><?php echo $heading_title_2; ?></h4>
          </div>
          <div class="col-sm-7">
            <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
                <input type="text" style="width:100%;font-size: 14px;" name="name_catalog" placeholder="Введите название каталога">
            </div>
            <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
                <input type="text" style="width:100%;font-size: 14px;" name="url_catalog" placeholder="Введите адресс каталога">
            </div>
          </div>
          <div class="col-sm-2" style="padding-right: 50px;padding-top: 45px;">
            <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></button>
          </div>
        </form>
      </div>
      <div class="row" style="padding:0;margin:0;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;padding-top:20px;padding-bottom: 20px;padding-left:20px;padding-right: 20px;">
        <table class="simple-little-table" cellspacing='0'>
            <tr>
                <th>Название каталога</th>
                <th></th>
                <th></th>
            </tr>
            <?php foreach ($all_catalog as $catalog) { ?>
              <tr>
                  <td><?php echo $catalog['name']; ?></td>
                  <td style="text-align: center;"><a href="<?php echo $catalog['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                  <td style="text-align: center;"><a href="<?php echo $catalog['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-primary"><i class="fa fa-remove"></i></a></td>
              </tr>
          <?php } ?>
        </table>
      </div>
    </div>
    <!-- Форма додавання нового каталога для завантаження -->
<?php echo $footer; ?>