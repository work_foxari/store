let mix = require('laravel-mix');

mix.webpackConfig({ devtool: "inline-source-map" });

mix
	.sass('catalog/view/theme/default/stylesheet/stylesheet.scss','catalog/view/theme/default/stylesheet/stylesheet.css')
    .sourceMaps()
    .options({
        processCssUrls: false
    })

	//.js('js/main.js', 'builded/main.js')
;